A POC how to run ProjectM visualization with Qt. For future integration in Sayonara Player.

Examples can be found here:

* https://github.com/projectM-visualizer/frontend-qt
* https://doc.qt.io/qt-5/qtgui-openglwindow-example.html

# Setup
## Get ProjectM and build 
* https://github.com/projectM-visualizer/projectm 
* https://github.com/projectM-visualizer/projectm/blob/master/BUILDING.md

# Additional sources
* Milkdrop presets: https://github.com/projectM-visualizer/presets-milkdrop-original/tree/master/Milkdrop-Original