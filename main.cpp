#include "openglwidget.h"
#include <QApplication>

int main(int argc, char** argv)
{
	auto app = QApplication(argc, argv);

	auto* window = new OpenGLWindow(nullptr);
	window->resize(640, 480);
	window->show();

	return app.exec(); // NOLINT(*-static-accessed-through-instance)
}
