#ifndef OPENGLWIDGET_H
#define OPENGLWIDGET_H

#include <QOpenGLWidget>
#include <QTimer>
#include <projectM-4/projectM.h>
#include <memory>

class OpenGLWindow :
	public QOpenGLWidget
{
	Q_OBJECT

	public:
		explicit OpenGLWindow(QWidget* parent = nullptr);
		~OpenGLWindow() noexcept override;

	protected:
		void initializeGL() override;
		void resizeGL(int w, int h) override;
		void paintGL() override;

	private slots:
		void updateData();

	private:
		std::unique_ptr<QTimer> m_timer {std::make_unique<QTimer>()};
		projectm* m_projectM {nullptr};
};

#endif
